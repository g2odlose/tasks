from task_11 import Dessert

class JellyBean(Dessert):
    def __init__(self, name="", calories=0, flavor=""):
        self.calories = calories
        self.name = name
        self.flavor = flavor
    def get_flavor(self, flavor=""):
        return self.flavor
    def set_flavor(self, flavor=""):
        self.flavor = flavor
    def is_delicious(self, flavor=""):
        if self.flavor == "black licorice":
            return False
        else:
            return True
