def multiply_numbers(inputs=None):
    answer = 1
    haveNumb = False #переменная для провери строки на число
    inputs_str = str(inputs)
    for element in inputs_str:
        if element.isdigit(): #если элемент число
            haveNumb = True
            answer = answer * int(element)
    if haveNumb:
        return answer
    else:
        return None
