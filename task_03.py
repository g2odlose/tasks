def max_odd(array):
    odd = []
    for element in array:
        try:
            element = float(element)
            if element % 3 == 0.0: #если число при делении на три имеет остаток равный 0 т.е. если число нечётное
                odd.append(int(element))
        except:
            pass
    try:
        return max(odd)
    except:
        odd = None
        return odd
