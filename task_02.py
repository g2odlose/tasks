import decimal #импортировать модуль для работы с десятичными
def coincidence(list_s=[], range_s=range(0,1)):
    def drange(x, y, jump): #функция range для float
        while x < y:
            yield float(x)
            x += decimal.Decimal(jump)
    answer = []
    range_a = list(range_s)
    if len(range_a) >= 2 and len(list_s) != 0: #если все параметры переданны правильно
        for element in list_s:
            if type(element) == float:
                if element in list(drange(1,4,'0.1')):
                    answer.append(element)
            if element in range_a:
                answer.append(element)
    return(answer)
