from string import punctuation #из модуля string получаем переменную со всеми знаками пунктуации
def is_palindrome(input):
    string = str(input)
    for p in punctuation:
        if p in string:
            string = string.replace(p, "")
    string = string.lower().replace(" ","")
    string_p = string[::-1] #создаём зеркальную копию string
    return string == string_p
