import datetime #импортируем модуль даты и времени
def date_in_future(int_day=0):
    today_date = datetime.datetime.now()
    if type(int_day) == int and int_day !=0:
        future_date = today_date + datetime.timedelta(days=int_day)
        return future_date.strftime("%d-%m-%Y %H:%M:%S")
    else:
        return today_date.strftime("%d-%m-%Y %H:%M:%S")
    
