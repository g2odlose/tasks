from string import punctuation #из модуля string получаем переменную со всеми знаками пунктуации
def count_words(string):
    answer = {}

    for p in punctuation:
        if p in string:
            string = string.replace(p, "")

    string_splited = string.lower().split()

    for element in string_splited:
        if element in answer:
            answer[element] = answer[element] + 1
        else:
            answer[element] = 1

    return answer
