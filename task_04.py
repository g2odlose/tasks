def sort_list(list):
    answer = []
    try:
        min_i = min(list)
        max_i = max(list)
    except:
        pass
    for element in list:
        if element == max_i:
            answer.append(min_i)
        elif element == min_i:
            answer.append(max_i)
        else:
            answer.append(element)
    try:
        answer.append(min(list))
    except:
        pass
    return answer
