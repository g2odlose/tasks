class WrongNumberOfPlayersError(Exception):#ошибка для неправильного числа игроков
    def __init__(self, *args):
        super().__init__(*args)
        self.msg = args[0] if args else None
    def __str__(self):
        return f"Ошибка: {self.msg}"
class NoSuchStrategyError(Exception): #ошибка для неправильной стратегии
    def __init__(self, *args):
        super().__init__(*args)
        self.msg = args[0] if args else None
    def __str__(self):
        return f"Ошибка: {self.msg}"
def rps_game_winner(match):
    player_1 = match[0] #переменная с первым игроком
    player_2 = match[1] #переменная со вторым игроком
    item_1 = player_1[1] #стратегия первого
    item_2 = player_2[1] #стратегия второго
    if (len(match) != 2):
        raise WrongNumberOfPlayersError ("Неверное количество игроков") #вызвать соответствующую ошибку
    if ((item_1 != "R" and item_1 != "P" and item_1 != "S") or (item_2 != "R" and item_2 != "P" and item_2 != "S")): #если выбрана неизвестная стратегия
        raise NoSuchStrategyError ("Неверно введина стратегия") #вызвать соответствующую ошибку
    else:
        if item_1 == "R":
            if item_2 == "R":
                winner = player_1
            elif item_2 == "P":
                winner = player_2
            else:
                winner = player_1
        elif item_1 == "P":
            if item_2 == "R":
                winner = player_1
            elif item_2 == "P":
                winner = player_1
            else:
                winner = player_2
        elif item_1 == "S":
            if item_2 == "R":
                winner = player_2
            elif item_2 == "P":
                winner = player_1
            else:
                winner = player_1
        output = winner[0] + " " + winner[1]
        return output

print(rps_game_winner([['player1', 'P'], ['player2', 'S'], ['player3', 'S']]))
