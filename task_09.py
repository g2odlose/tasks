def connect_dicts(dict1, dict2):

    dict1_int = 0
    dict2_int = 0

    for dict_integer in dict1.keys():
        dict1_int = dict1_int + dict1[dict_integer]
    for dict_integer in dict2.keys():
        dict2_int = dict2_int + dict2[dict_integer]

    if dict1_int - dict2_int < 0:
        dict1.update(dict2)
    elif dict1_int - dict2_int > 0:
        dict2.update(dict1)
    elif dict1_int - dict2_int == 0:
        dict1.update(dict2)

    unidict = {**dict2, **dict1}

    for key in unidict.copy():
        if unidict[key] < 10:
            del unidict[key]

    sorted_unidict = sorted(unidict, key=unidict.get)

    answer = {}
    for element in sorted_unidict:
        answer[element] = unidict[element]

    return answer
