def combine_anagrams(words_array):
    answer = {}
    for element in words_array:
        anagram = "".join(sorted(element.lower()))
        if anagram in answer:
            answer[anagram].append(element)
        else:
            answer[anagram] = [element]
    return list(answer.values())
